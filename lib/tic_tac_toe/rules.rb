module TicTacToe
  class Rules
    def win?(board, player1_symbol, player2_symbol)
      winning_symbol?(board, player1_symbol) ||
        winning_symbol?(board, player2_symbol)
    end

    def winning_symbol?(board, symbol)
      lines(board).any? do |line|
        line.all? { |x| x == symbol }
      end
    end

    def game_over?(board, player1_symbol, player2_symbol)
      win?(board, player1_symbol, player2_symbol) ||
        tie?(board, player1_symbol, player2_symbol)
    end

    def tie?(board, player1_symbol, player2_symbol)
      board.all? { |s| s == player1_symbol || s == player2_symbol }
    end

    private

    def lines(board_array)
      board_width = Math.sqrt(board_array.length)
      rows = board_array_to_rows(board_array, board_width)
      rows + columns(rows) + diagonals(rows)
    end

    def board_array_to_rows(board_array, board_width)
      board_array.each_slice(board_width).to_a
    end

    def columns(rows)
      rows.transpose
    end

    def diagonals(rows)
      diagonal_left(rows) + diagonal_right(rows)
    end

    def diagonal_left(rows)
      [rows.map.with_index { |row, index| row[index] }]
    end

    def diagonal_right(rows)
      [rows.map.with_index { |row, index| row[row.length - 1 - index] }]
    end
  end
end

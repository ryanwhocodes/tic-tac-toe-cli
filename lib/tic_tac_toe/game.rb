require_relative './rules.rb'

module TicTacToe
  class Game
    HUMAN_VS_HUMAN = '1'.freeze
    HUMAN_VS_COMPUTER = '2'.freeze
    COMPUTER_VS_COMPUTER = '3'.freeze
    DEFAULT_PLAYER_1_SYMBOL = 'X'.freeze
    DEFAULT_PLAYER_2_SYMBOL = 'O'.freeze
    DEFAULT_BOARD_WIDTH = '3'.freeze

    attr_reader :player1_symbol, :player2_symbol, :rules, :winner,
                :current_player, :game_type, :computer_player

    def initialize(options = {})
      options = defaults.merge(options)
      @rules = options[:rules]
      @board = Array.new(options[:board_width].to_i**2)
      @computer_player = options[:computer_player]
      @game_type = options[:game_type]
      @player1_symbol = options[:symbol1]
      @player2_symbol = options[:symbol2]
      @current_player = options[:first_player_symbol]
    end

    def defaults
      {
        game_type: HUMAN_VS_HUMAN,
        symbol1: DEFAULT_PLAYER_1_SYMBOL,
        symbol2: DEFAULT_PLAYER_2_SYMBOL,
        first_player_symbol: DEFAULT_PLAYER_1_SYMBOL,
        rules: Rules.new,
        board_width: DEFAULT_BOARD_WIDTH,
        computer_player: ComputerPlayer.new
      }
    end

    def play_move(move, symbol)
      @board[move] = symbol
      switch_turns unless game_is_over?
    end

    def current_player_human?
      case game_type
      when HUMAN_VS_HUMAN
        true
      when HUMAN_VS_COMPUTER
        current_player == player1_symbol
      when COMPUTER_VS_COMPUTER
        false
      end
    end

    def generate_computer_move
      computer_player.get_computer_spot(board, opponent_symbol, current_player)
    end

    def opponent_symbol
      current_player == player1_symbol ? player2_symbol : player1_symbol
    end

    def switch_turns
      @current_player = opponent_symbol
    end

    def game_is_over?(b = board)
      return true if @winner
      if rules.win?(b, player1_symbol, player2_symbol)
        @winner = current_player
        return true
      end
      rules.tie?(b, player1_symbol, player2_symbol)
    end

    def board
      @board.dup
    end

    def available_spaces
      board.each_index.select { |index| board[index].nil? }
    end
  end
end

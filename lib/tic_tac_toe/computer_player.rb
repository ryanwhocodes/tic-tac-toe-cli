# TicTacToe module
require_relative './rules.rb'
module TicTacToe
  class ComputerPlayer
    DEFAULT_RULES = TicTacToe::Rules.new
    DIFFICULTY_EASY = 0
    DIFFICULTY_MEDIUM = 0.5
    DIFFICULTY_HARD = 1
    DEFAULT_PROBABILITY = DIFFICULTY_MEDIUM
    START_DEPTH = 0
    MAX_DEPTH = 6
    MAX_SCORE = 100

    def initialize(probability_of_best_move = DEFAULT_PROBABILITY,
                   rules = DEFAULT_RULES)
      @rules = rules
      @probability = probability_of_best_move
    end

    def get_computer_spot(board, opponent_symbol, computer_symbol)
      @board = board
      @opp = opponent_symbol
      @com = computer_symbol
      r = rand
      r <= @probability ? get_best_move(board, @com) : random_move(board)
    end

    private

    attr_reader :rules, :board

    def get_available_spaces(b)
      b.each_index.select { |index| b[index].nil? }
    end

    def random_move(board)
      get_available_spaces(board).sample
    end

    def get_best_move(board, computer_symbol)
      @best_score = {}
      negamax(board, computer_symbol)
      pick_best_score
    end

    def pick_best_score
      @best_score.max_by { |_k, v| v }[0]
    end

    def score(new_board, depth)
      return -MAX_SCORE / depth if rules.winning_symbol?(new_board, @opp)
      return MAX_SCORE / depth if rules.winning_symbol?(new_board, @com)
      0
    end

    def opponent(player)
      player == @com ? @opp : @com
    end

    def get_new_board(board, index, player)
      new_board = board.dup
      new_board[index] = player
      new_board
    end

    def negamax(board, player, depth = START_DEPTH,
                alpha = -MAX_SCORE, beta = MAX_SCORE, color = 1)
      if rules.game_over?(board, @com, @opp) || depth > MAX_DEPTH
        return color * score(board, depth)
      end

      max = -MAX_SCORE
      get_available_spaces(board).each do |as|
        new_board = get_new_board(board, as, player)
        v = -negamax(new_board, opponent(player), depth+1, -beta, -alpha, -color)
        max = [max, v].max
        @best_score[as] = max if depth == START_DEPTH
        alpha = [alpha, v].max
        return alpha if alpha >= beta
      end

      max
    end
  end
end

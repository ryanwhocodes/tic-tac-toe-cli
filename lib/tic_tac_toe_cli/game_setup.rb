module TicTacToeCLI
  class GameSetup
    MESSAGES = {
      welcome: 'Welcome to Tic Tac Toe!',
      choose_game_type: ['Enter a game type:',
                         '1: human vs human',
                         '2: human vs computer',
                         '3: computer vs computer'].join("\n"),
      choose_first_symbol: 'Enter a capital letter for Player 1',
      choose_second_symbol: 'Enter a different capital letter for Player 2',
      choose_first_player: 'Enter the symbol of the player to go first',
      try_again: 'Please try again...',
      you_chose: 'You chose ',
      game_starting: "Starting Tic Tac Toe\n"
    }.freeze

    DATA = {
      game_types: [*'1'..'3'],
      symbols: [*'A'..'Z']
    }.freeze

    attr_reader :input, :output, :chosen_options

    def initialize(input = $stdin, output = $stdout)
      @input = input
      @output = output
      @chosen_options = {}
    end

    def choose_options!
      print_welcome
      choose_game_type
      choose_first_symbol
      choose_second_symbol
      choose_first_player
      print_game_starting
    end

    def print_welcome
      output.puts MESSAGES[:welcome]
    end

    def print_game_starting
      output.puts MESSAGES[:game_starting]
    end

    def choose_game_type
      get_user_choice(:game_type, MESSAGES[:choose_game_type],
                      DATA[:game_types])
    end

    def choose_first_symbol
      get_user_choice(:symbol1, MESSAGES[:choose_first_symbol], DATA[:symbols])
    end

    def choose_second_symbol
      get_user_choice(:symbol2, MESSAGES[:choose_first_symbol],
                      DATA[:symbols] - [chosen_options[:symbol1]])
    end

    def choose_first_player
      get_user_choice(:first_player_symbol, MESSAGES[:choose_first_player],
                      [chosen_options[:symbol1], chosen_options[:symbol2]])
    end

    def get_user_choice(option_name, prompt_string, valid_data_array)
      output.puts prompt_string
      choice = get_valid_input(valid_data_array)
      chosen_options[option_name.to_sym] = choice
    end

    private

    def get_valid_input(options)
      choice = input.gets.chomp
      if options.include?(choice)
        output.puts MESSAGES[:you_chose] + choice
        choice
      else
        output.puts MESSAGES[:try_again]
        get_valid_input(options)
      end
    end
  end
end

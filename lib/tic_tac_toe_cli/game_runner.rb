require_relative '../tic_tac_toe/computer_player.rb'

module TicTacToeCLI
  class GameRunner
    MESSAGES = {
      current_turn: 'Current turn: player ',
      turn_choices: 'Please enter a choice from: ',
      you_chose: 'You chose ',
      try_again: 'Please try again...',
      thinking: 'The computer is thinking...',
      computer_chose: 'The computer chose ',
      win: ' is the winner!',
      tie: 'It\'s a tie!'
    }.freeze

    attr_reader :game, :input, :output

    def initialize(input = $stdin,
                   output = $stdout,
                   game = TicTacToe::Game.new,
                   board_view = BoardView.new)
      @input = input
      @output = output
      @game = game
      @board_view = board_view
    end

    def start_game
      print_board(game.board)
      play_turn until game.game_is_over?
      print_board(game.board)
      print_outcome
    end

    def play_turn
      print_current_turn(game.current_player)
      print_turn_choices(game.available_spaces)
      game.current_player_human? ? play_human_turn : play_computer_turn
      game.play_move(@spot, game.current_player)
      print_board(game.board)
    end

    def play_human_turn
      @spot = get_human_spot(game.available_spaces,
                             game.current_player,
                             game.opponent_symbol)
    end

    def play_computer_turn
      print_computer_is_thinking
      @spot = game.generate_computer_move
      print_computer_move(@spot)
    end

    def print_computer_is_thinking
      output.puts MESSAGES[:thinking]
    end

    def print_computer_move(spot)
      output.puts MESSAGES[:computer_chose] + spot.to_s
    end

    def print_turn_choices(available_spaces)
      output.puts MESSAGES[:turn_choices] + available_spaces.join(', ')
    end

    def print_current_turn(symbol)
      output.puts MESSAGES[:current_turn] + symbol
    end

    def print_board(board)
      output.puts @board_view.format_board(board)
    end

    def print_outcome
      output.puts game.winner ? game.winner + MESSAGES[:win] : MESSAGES[:tie]
    end

    def get_human_spot(available_spaces, symbol1, symbol2)
      spot = input.gets
      spot = spot.chomp.to_i
      if available_spaces.include?(spot)
        output.puts MESSAGES[:you_chose] + spot.to_s
        return spot
      end
      output.puts MESSAGES[:try_again]
      get_human_spot(available_spaces, symbol1, symbol2)
    end
  end
end

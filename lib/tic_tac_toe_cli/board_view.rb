module TicTacToeCLI
  class BoardView
    def format_board(board)
      @board_width = Math.sqrt(board.length)
      @longest_number = board.length.to_s.length
      board_with_indexes = add_indexes_to_empty_spaces(board)
      rows = split_into_rows(board_with_indexes)
      formatted_rows = add_separators_to_lines(rows)
      formatted_board = add_separator_to_rows(formatted_rows)
      "\n" + formatted_board + "\n\n"
    end

    private

    attr_reader :board_width, :longest_number

    def add_indexes_to_empty_spaces(board)
      board.map.with_index { |cell, index| cell.nil? ? index : cell }
    end

    def split_into_rows(board)
      board.each_slice(board_width).to_a
    end

    def add_separators_to_lines(rows)
      rows.map do |row|
        row.map do |s|
          s = s.to_s
          s << ' ' while s.length < longest_number
          " #{s} "
        end.join('|')
      end
    end

    def add_separator_to_rows(rows)
      single_separator = '='
      single_separator << '=' while single_separator.length < longest_number
      single_separator << '=='
      row_separator = Array.new(board_width, single_separator).join('+')
      rows.join("\n" + row_separator + "\n")
    end
  end
end

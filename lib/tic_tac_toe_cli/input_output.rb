module TicTacToeCLI
  class InputOutput
    def initialize(input, output)
      @input = input
      @output = output
    end

    def puts(string)
      @output.puts string
    end

    def gets
      @input.gets
    end
  end
end

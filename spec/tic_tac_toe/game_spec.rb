require 'spec_helper'

module TicTacToe
  describe Game do
    let(:game_type)         { '2'                                     }
    let(:symbol1)           { 'X'                                     }
    let(:symbol2)           { 'O'                                     }
    subject(:game)          { described_class.new(game_type: '2')     }
    let(:mock_board_array)  { [*0..8]                                 }
    let(:human_vs_human)    { { game_type: '1' }                      }
    let(:comp_vs_comp)      { { game_type: '3' }                      }

    describe '#play_move' do
      it 'inputs move into the board' do
        game.play_move(1, symbol1)
        expect(game.board[1]).to eq symbol1
      end

      it 'switches players' do
        expect(game.current_player).to eq symbol1
        game.play_move(1, symbol1)
        expect(game.current_player).to eq symbol2
      end
    end

    describe '#current_player_human?' do
      context 'human v human' do
        it 'returns true' do
          expect(described_class.new(human_vs_human).current_player_human?)
            .to eq true
        end

        context 'switch_turns' do
          it 'returns true' do
            game.switch_turns
            expect(described_class.new(human_vs_human).current_player_human?)
              .to eq true
          end
        end
      end

      context 'human v computer' do
        context 'current_player is human' do
          it 'returns true' do
            expect(game.current_player_human?)
              .to eq true
          end
        end

        context 'current_player is computer' do
          it 'returns false' do
            game.switch_turns
            expect(game.current_player_human?)
              .to eq false
          end
        end
      end

      context 'computer vs computer game' do
        it 'returns false' do
          expect(described_class.new(comp_vs_comp).current_player_human?)
            .to eq false
        end

        context 'switch_turns' do
          it 'returns false' do
            game.switch_turns
            expect(described_class.new(comp_vs_comp).current_player_human?)
              .to eq false
          end
        end
      end
    end

    describe 'new' do
      context 'custom symbols' do
        context 'symbol1' do
          it 'returns the symbols chosen' do
            expect(game.player1_symbol).to eq symbol1
          end
        end

        context 'symbol2' do
          it 'returns the symbols chosen' do
            expect(game.player2_symbol).to eq symbol2
          end
        end
      end
    end

    describe '#available_spaces' do
      context 'empty board' do
        it 'returns spaces on the board that do not equal player symbols' do
          expect(game.available_spaces).to eq mock_board_array
        end
      end

      context 'first two places played' do
        before do
          game.play_move(0, 'O')
          game.play_move(1, 'X')
        end
        it 'returns spaces on the board that do not equal player symbols' do
          expect(game.available_spaces).to eq [*2..8]
        end
      end
    end
  end
end

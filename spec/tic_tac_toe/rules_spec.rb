require 'spec_helper'

module TicTacToe
  describe Rules do
    let(:symbol1)             { 'X'                                           }
    let(:symbol2)             { 'O'                                           }
    subject(:rules)           { described_class.new                           }
    let(:board_array)         { [Array.new(9)]                                }
    let(:draw_board)          { ['X', 'O', 'X', 'O', 'X', 'X', 'O', 'X', 'O'] }
    let(:board_no_win)        { ['X', 'O', nil, nil, 'X', 'O', 'O', nil, nil] }
    let(:board_win_top_row)   { ['X', 'X', 'X', nil, nil, nil, nil, nil, nil] }
    let(:board_win_mid_row)   { [nil, nil, nil, 'X', 'X', 'X', nil, nil, nil] }
    let(:board_win_bot_row)   { [nil, nil, nil, nil, nil, nil, 'X', 'X', 'X'] }
    let(:board_win_l_col)     { ['X', nil, nil, 'X', nil, nil, 'X', nil, nil] }
    let(:board_win_mid_col)   { [nil, 'X', nil, nil, 'X', nil, nil, 'X', nil] }
    let(:board_win_r_col)     { [nil, nil, 'X', nil, nil, 'X', nil, nil, 'X'] }
    let(:board_diag_left)     { ['X', nil, nil, nil, 'X', nil, nil, nil, 'X'] }
    let(:board_diag_right)    { [nil, nil, 'X', nil, 'X', nil, 'X', nil, nil] }

    describe '#tie?' do
      context 'board not full' do
        it 'returns false' do
          expect(rules.tie?(board_array, symbol1, symbol2))
            .to eq false
        end
      end

      context 'board full' do
        it 'returns true' do
          expect(rules.tie?(draw_board, symbol1, symbol2))
            .to eq true
        end
      end
    end

    describe '#winning_symbol?' do
      context 'non winning board' do
        it 'returns false' do
          expect(rules.winning_symbol?(board_no_win, symbol2))
            .to eq false
        end
      end

      context 'winning line' do
        context 'top row' do
          it 'returns true' do
            expect(rules.winning_symbol?(board_win_top_row, symbol1))
              .to eq true
          end
        end
      end
    end

    describe '#win?' do
      context 'non winning board' do
        it 'returns false' do
          expect(rules.win?(board_no_win, symbol1, symbol2))
            .to eq false
        end
      end

      context 'winning line' do
        context 'top row' do
          it 'returns true' do
            expect(rules.win?(board_win_top_row, symbol1, symbol2))
              .to eq true
          end
        end

        context 'mid row' do
          it 'returns true' do
            expect(rules.win?(board_win_mid_row, symbol1, symbol2))
              .to eq true
          end
        end

        context 'bottom row' do
          it 'returns true' do
            expect(rules.win?(board_win_bot_row, symbol1, symbol2))
              .to eq true
          end
        end

        context 'left column' do
          it 'returns true' do
            expect(rules.win?(board_win_l_col, symbol1, symbol2))
              .to eq true
          end
        end

        context 'mid column' do
          it 'returns true' do
            expect(rules.win?(board_win_mid_col, symbol1, symbol2))
              .to eq true
          end
        end

        context 'right column' do
          it 'returns true' do
            expect(rules.win?(board_win_r_col, symbol1, symbol2))
              .to eq true
          end
        end

        context 'diagonal left' do
          it 'returns true' do
            expect(rules.win?(board_diag_left, symbol1, symbol2))
              .to eq true
          end
        end

        context 'diagonal right' do
          it 'returns true' do
            expect(rules.win?(board_diag_right, symbol1, symbol2))
              .to eq true
          end
        end
      end
    end

    describe '#game_over?' do
      context 'no win or draw' do
        it 'returns false' do
          expect(rules.game_over?(board_array, symbol1, symbol2))
            .to eq false
        end
      end

      context 'draw' do
        it 'returns true' do
          expect(rules.game_over?(draw_board, symbol1, symbol2))
            .to eq true
        end
      end

      context 'win' do
        it 'returns true' do
          expect(rules.game_over?(board_diag_right, symbol1, symbol2))
            .to eq true
        end
      end
    end
  end
end

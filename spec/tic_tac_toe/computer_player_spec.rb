require 'tic_tac_toe/computer_player'

module TicTacToe
  describe ComputerPlayer do
    let(:hard_difficulty)     { described_class::DIFFICULTY_HARD              }
    let(:computer_hard)       { described_class.new(hard_difficulty)          }
    let(:computer_medium)     { described_class.new                           }
    let(:comp_symbol)         { 'X'                                           }
    let(:opponent_symbol)     { 'O'                                           }
    let(:board_center)        { 4                                             }
    let(:winning_board)       { ['X', 'X', nil, nil, 'O', nil, nil, nil, nil] }
    let(:winning_board_col)   { ['X', 'O', 'X', nil, 'O', 'X', nil, nil, nil] }
    let(:full_board)          { Array.new(9, 'X')                             }
    let(:winning_move)        { 2                                             }
    let(:board_center_taken)  { [nil, nil, nil, nil, 'X', nil, nil, nil, nil] }
    let(:winning_board_diag)  { ['X', nil, nil, nil, 'X', nil, nil, nil, nil] }
    let(:diag_winning_move)   { 8                                             }
    let(:all_spaces)          { Array.new(9)                                  }
    let(:indexes_no_center)   { [0, 1, 2, 3, 5, 6, 7, 8]                      }

    describe '#get_computer_spot' do
      before { srand(1000) }

      context 'hard difficulty, calls minimax method to get best move' do
        context 'winning move on the board' do
          context 'top row has winning move' do
            it 'returns winning move' do
              expect(computer_hard.get_computer_spot(winning_board,
                                                     opponent_symbol,
                                                     comp_symbol))
                .to eq winning_move
            end
          end

          context 'diagonal line has winning move' do
            it 'returns winning move' do
              expect(computer_hard.get_computer_spot(winning_board_diag,
                                                     opponent_symbol,
                                                     comp_symbol))
                .to eq diag_winning_move
            end
          end

          context 'column line has winning move' do
            it 'returns winning move' do
              expect(computer_hard.get_computer_spot(winning_board_col,
                                                     opponent_symbol,
                                                     comp_symbol))
                .to eq diag_winning_move
            end
          end
        end

        context 'empty board' do
          it 'return the first corner (spot 0)' do
            expect(computer_hard
              .get_computer_spot(all_spaces, opponent_symbol, comp_symbol))
              .to eq 0
          end
        end

        context 'board with center taken' do
          it 'does not return center space' do
            expect(computer_hard
              .get_computer_spot(board_center_taken, opponent_symbol,
                                 comp_symbol))
              .not_to eq board_center
          end
          it 'return random move' do
            expect(indexes_no_center)
              .to include computer_hard.get_computer_spot(board_center_taken,
                                                          opponent_symbol,
                                                          comp_symbol)
          end
        end
      end

      context 'medium difficulty, 50% chance of #minimax or #random_move' do
        context 'winning move on the board' do
          context 'top row has winning move' do
            it 'returns winning move with 50% probability' do
              expect(computer_medium
              .get_computer_spot(winning_board, opponent_symbol, comp_symbol))
                .to eq winning_move
              expect(computer_medium
              .get_computer_spot(winning_board, opponent_symbol, comp_symbol))
                .not_to eq winning_move
              expect(computer_medium
              .get_computer_spot(winning_board, opponent_symbol, comp_symbol))
                .to eq winning_move
              expect(computer_medium
              .get_computer_spot(winning_board, opponent_symbol, comp_symbol))
                .not_to eq winning_move
            end
          end
        end
      end
    end
  end
end

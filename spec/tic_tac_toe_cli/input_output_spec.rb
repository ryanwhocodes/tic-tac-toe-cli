require 'spec_helper'

module TicTacToeCLI
  describe InputOutput do
    let(:string)    { "test\n"                                            }
    let(:io)        { described_class.new($stdin, $stdout)                }
    let(:io_input)  { described_class.new(StringIO.new(string), $stdout)  }

    describe 'puts' do
      it 'outputs the string' do
        expect { io.puts string }.to output(string).to_stdout
      end
    end

    describe 'gets' do
      it 'gets input' do
        expect(io_input.gets).to eq string
      end
    end
  end
end

require 'spec_helper'

module TicTacToeCLI
  describe GameSetup do
    let(:game_setup)            { described_class.new                          }
    let(:computer_vs_computer)  { "3\n3\nX\nO\nX\n"                            }
    let(:invalid_input)         { "4\n3\n3\nX\nO\nX\n"                         }
    let(:human_vs_human)        { "1\n3\nx\nX\nO\nX\nX\n"                      }
    let(:output_io)             { StringIO.new                                 }

    def game_setup_io(string)
      input_io = StringIO.new(string)
      described_class.new(input_io, output_io)
    end

    describe '#choose_options!' do
      context 'valid input' do
        it 'confirms user choice' do
          game_setup_io(human_vs_human).choose_options!
          expect(output_io.string)
            .to include described_class::MESSAGES[:you_chose] + 'O'
        end
      end

      context 'invalid input' do
        it 'outputs try again...' do
          game_setup_io(invalid_input).choose_options!
          expect(output_io.string)
            .to include described_class::MESSAGES[:try_again]
        end
      end

      it 'outputs confirmation of a new game' do
        game_setup_io(human_vs_human).choose_options!
        expect(output_io.string)
          .to include described_class::MESSAGES[:game_starting]
      end
    end

    describe '#print_welcome' do
      it 'outputs Tic Tac Toe' do
        game_setup_io(computer_vs_computer).print_welcome
        expect(output_io.string)
          .to include described_class::MESSAGES[:welcome]
      end
    end

    describe '#print_game_starting' do
      it 'outputs confirmation of a new game with players and their types' do
        expect { game_setup.print_game_starting }
          .to output(described_class::MESSAGES[:game_starting]).to_stdout
      end
    end

    describe '#choose_game_type' do
      it 'outputs choose game type options' do
        game_setup_io(human_vs_human).choose_game_type
        expect(output_io.string)
          .to include described_class::MESSAGES[:choose_game_type]
      end
    end

    describe '#choose_first_symbol' do
      it 'outputs choose symbol for first player' do
        game_setup_io(human_vs_human).choose_first_symbol
        expect(output_io.string)
          .to include described_class::MESSAGES[:choose_first_symbol]
      end

      context 'input is a capital letter' do
        it 'outputs confirmation of the symbol chosen' do
          game_setup_io(human_vs_human).choose_first_symbol
          expect(output_io.string)
            .to include described_class::MESSAGES[:you_chose]
        end
      end

      context 'input is not a capital letter' do
        it 'outputs Please try again...' do
          game_setup_io("0\n$\n]\nX\n1\nX\nC\nX\n").choose_first_symbol
          expect(output_io.string)
            .to include described_class::MESSAGES[:try_again]
        end
      end
    end

    describe '#choose_first_player' do
      it 'outputs choose first player' do
        game_setup_io(human_vs_human).choose_options!
        expect(output_io.string)
          .to include described_class::MESSAGES[:choose_first_player]
      end

      context 'input a capital letter' do
        it 'outputs confirmation of the player symbol chosen' do
          game_setup_io(human_vs_human).choose_options!
          expect(output_io.string)
            .to include described_class::MESSAGES[:you_chose] + 'X'
        end
      end

      context 'input is not a capital letter or one of player symbols' do
        it 'outputs Please try again...' do
          game_setup_io(invalid_input).choose_options!
          expect(output_io.string)
            .to include described_class::MESSAGES[:try_again]
        end
      end
    end
  end
end

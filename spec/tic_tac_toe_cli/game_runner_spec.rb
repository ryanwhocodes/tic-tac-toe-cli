require 'spec_helper'

module TicTacToeCLI
  describe GameRunner do
    subject(:game_runner)   { described_class.new                             }
    let(:symbol1)           { 'X'                                             }
    let(:symbol2)           { 'O'                                             }
    let(:mock_board_array)  { Array.new(9)                                    }
    let(:available_spaces)  { [0, 1, 2]                                       }
    let(:space_choices)     { available_spaces.join(', ') + "\n"              }
    let(:invalid_input)     { "Y\n0\n1\n2\n3\n4\n5\n6\n7\n8"                  }
    let(:winning_game)      { "0\n1\n2\n3\n4\n5\n6\n7\n8"                     }
    let(:winning_game2)     { "0\n4\n5\n6\n7\n8\n1\n2\n3\n"                   }
    let(:tie_game)          { [0, 1, 2, 3, 5, 4, 6, 8, 7].join("\n")          }
    let(:spot)              { "1\n"                                           }
    let(:mock_board_no_win) { ['X', 'O', nil, nil, 'X', 'O', 'O', nil, nil]   }
    let(:turn_string)       do
      described_class::MESSAGES[:turn_choices] + space_choices
    end
    let(:thinking_string)   { described_class::MESSAGES[:thinking] + "\n" }
    let(:board_string)      do
      ["\n",
       " 0 | 1 | 2 \n",
       "===+===+===\n",
       " 3 | 4 | 5 \n",
       "===+===+===\n",
       " 6 | 7 | 8 \n\n"]
        .join
    end
    let(:output_io)       { StringIO.new                                      }
    before                { srand(1)                                          }

    def setup_app(string = '')
      input_io = StringIO.new(string)
      described_class.new(input_io, output_io)
    end

    describe '#start_game' do
      context 'a tie game' do
        it 'declares a tie' do
          setup_app(tie_game).start_game
          expect(output_io.string).to include described_class::MESSAGES[:tie]
        end
      end

      context 'a winning game' do
        it 'declares a winner' do
          setup_app(winning_game).start_game
          expect(output_io.string).to include described_class::MESSAGES[:win]
        end
      end
    end

    describe '#get_human_spot' do
      context 'valid input' do
        it 'declares a tie' do
          setup_app(tie_game).start_game
          expect(output_io.string)
            .to include described_class::MESSAGES[:you_chose]
        end
      end

      context 'invalid input' do
        it 'declares a winner' do
          setup_app(invalid_input).start_game
          expect(output_io.string)
            .to include described_class::MESSAGES[:try_again]
        end
      end
    end

    describe '#play_computer_turn' do
      srand(1)
      it 'outputs computer is thinking' do
        setup_app(winning_game).play_computer_turn
        expect(output_io.string)
          .to include described_class::MESSAGES[:thinking]
      end

      it 'outputs computer choice' do
        setup_app(winning_game).play_computer_turn
        expect(output_io.string)
          .to include described_class::MESSAGES[:computer_chose]
      end

      it 'confirms user chosen spot' do
        setup_app(winning_game).start_game
        expect(output_io.string)
          .to include described_class::MESSAGES[:you_chose]
      end
    end

    describe '#print_computer_move' do
      it 'prints computer is thinking' do
        expect { game_runner.print_computer_move(spot) }
          .to output(described_class::MESSAGES[:computer_chose] + spot)
          .to_stdout
      end
    end

    describe '#print_turn_choices' do
      it 'prints turn choices' do
        expect { game_runner.print_turn_choices(available_spaces) }
          .to output(turn_string).to_stdout
      end
    end

    describe '#print_outcome' do
      context 'a tie game' do
        it 'declares a tie' do
          setup_app(tie_game).print_outcome
          expect(output_io.string).to include described_class::MESSAGES[:tie]
        end
      end
    end

    describe '#print_board' do
      it 'prints formatted baord' do
        expect { game_runner.print_board(mock_board_array) }
          .to output(board_string).to_stdout
      end
    end

    describe '#print_computer_is_thinking' do
      it 'prints computer is thinking message' do
        expect { game_runner.print_computer_is_thinking }
          .to output(described_class::MESSAGES[:thinking] + "\n").to_stdout
      end
    end
  end
end

require 'spec_helper'

module TicTacToeCLI
  describe BoardView do
    let(:board_view)        { described_class.new                             }
    let(:mock_board_array)  { Array.new(9)                                    }
    let(:mock_board_no_win) { ['X', 'O', nil, nil, 'X', 'O', 'O', nil, nil]   }
    let(:empty_board_string) do
      ["\n",
       " 0 | 1 | 2 \n",
       "===+===+===\n",
       " 3 | 4 | 5 \n",
       "===+===+===\n",
       " 6 | 7 | 8 \n\n"]
        .join
    end
    let(:board_string_with_symbols) do
      ["\n",
       " X | O | 2 \n",
       "===+===+===\n",
       " 3 | X | O \n",
       "===+===+===\n",
       " O | 7 | 8 \n\n"]
        .join
    end

    describe '#format_board' do
      context 'empty board' do
        it 'returns formatted board string with indexes for empty spaces' do
          expect(board_view.format_board(mock_board_array))
            .to eq empty_board_string
        end
      end

      context 'board with some spaces taken' do
        it 'returns formatted board sting with symbols for spaces taken' do
          expect(board_view.format_board(mock_board_no_win))
            .to eq board_string_with_symbols
        end
      end
    end
  end
end

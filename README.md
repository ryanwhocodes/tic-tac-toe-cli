# Tic Tac Toe CLI

A tech test to refactor a command line tic tac toe game (Ruby).

![tic-tac-toe.png](images/tic-tac-toe.png)

[Instructions](#instructions) &nbsp;&middot;&nbsp;
[Example Game](#example-game) &nbsp;&middot;&nbsp;
[Tests](#tests) &nbsp;&middot;&nbsp;
[Specification](#specification) &nbsp;&middot;&nbsp;
[Code Review Criteria](#code-review-criteria) &nbsp;&middot;&nbsp;
[Approach](#approach) &nbsp;&middot;&nbsp;
[Project Setup](#project-setup) &nbsp;&middot;&nbsp;
[User Stories](#user-stories) &nbsp;&middot;&nbsp;
[Classes](#classes) &nbsp;&middot;&nbsp;
[Design Principles](#design-principles) &nbsp;&middot;&nbsp;
[Development](#development)

### Instructions

Open your terminal and navigate to a directory to install the program.

If you haven't already, install [bundler](http://bundler.io/) with `gem install bundler`.

```
git clone git@github.com:ryanwhocodes/tic_tac_toe_cli.git
cd tic_tac_toe_cli
bundle install
bin/tic_tac_toe
```

**Game Rules**
* Players take turns to place their symbol on the board
* You cannot take a space that is already taken
* The winner is the first to have matching symbols in a row, column or diagonal
* A tie is when the board is full but there are no winning lines

See [example game](#example-game) to see how to play.

### Example Game

```
Welcome to Tic Tac Toe!
Enter a game type
1: human vs human
2: human vs computer
3: computer vs computer
2
You chose 2
Enter a capital letter for Player 1
H
You chose H
Enter a capital letter for Player 1
C
You chose C
Enter the symbol of the player to go first
C
You chose C
Starting Tic Tac Toe

 0 | 1 | 2
===+===+===
 3 | 4 | 5
===+===+===
 6 | 7 | 8

Current turn: player C
Please enter a choice from: 0, 1, 2, 3, 4, 5, 6, 7, 8
The computer is thinking...
The computer chose 4

 0 | 1 | 2
===+===+===
 3 | C | 5
===+===+===
 6 | 7 | 8

Current turn: player H
Please enter a choice from: 0, 1, 2, 3, 5, 6, 7, 8
1
You chose 1

 0 | H | 2
===+===+===
 3 | C | 5
===+===+===
 6 | 7 | 8

Current turn: player C
Please enter a choice from: 0, 2, 3, 5, 6, 7, 8
The computer is thinking...
The computer chose 8

 0 | H | 2
===+===+===
 3 | C | 5
===+===+===
 6 | 7 | C

Current turn: player H
Please enter a choice from: 0, 2, 3, 5, 6, 7
5
You chose 5

 0 | H | 2
===+===+===
 3 | C | H
===+===+===
 6 | 7 | C

Current turn: player C
Please enter a choice from: 0, 2, 3, 6, 7
The computer is thinking...
The computer chose 0

 C | H | 2
===+===+===
 3 | C | H
===+===+===
 6 | 7 | C


 C | H | 2
===+===+===
 3 | C | H
===+===+===
 6 | 7 | C

C is the winner!
```

### Tests

Run `rspec` in your terminal.

Tests are stored in the `spec` folder.

You can also view the build and tests on [Travis CI](https://travis-ci.org/ryanwhocodes/tic_tac_toe_cli).

### Specification

Hello,

I’m the project manager at a Command Line Games, Inc. I have a small dev team and we hired a consulting company to help us build an app that will feature a number of games for children, one being Tic Tac Toe.  

They just demoed the basic version of the Tic Tac Toe game in the console and my boss wasn’t thrilled with what he saw. The game play was rough. It didn’t function as he expected. We’ve decided to move in a different direction and bring in someone else. While my boss doesn’t have a technical background, I do, and we both understand the importance of writing code that can be maintained in the future.

We would like you to improve the existing Tic Tac Toe that the previous firm worked on. There are a number of issues with the code. Below I’ve listed some of those issues, but I’m sure there are more.

* The game does not gracefully handle bad user input.
* In its current form, it’s supposed to be played at a difficulty level of “hard”, meaning the computer player cannot be beaten. In reality, however, the computer player can be beaten in certain situations. This is more like a “medium” difficulty level.
* The game play left a lot to be desired. The user messages are lacking. They’re unclear. It’s confusing to see the spot that’s selected and the board all on the screen. It’s easy to get lost in what’s happening. It’s weird the way the computer picks its spot without notifying the user.

As you can tell, there are a lot of problems and from what our devs say, the code itself is a mess. It’s untested and therefore unmaintainable. It’s poorly­-written and inflexible. This puts us in a difficult position because we have a number of features we would like to add and we’re hoping you can help.   

We hope that you’ll be able to help us get the code in a better state. Without that, our devs don’t even think we’ll be able to implement the new features my boss has requested. For one thing, the existing code is so coupled to the console that implementing any other UI is nearly impossible! Below you’ll see a list of the features we’re hoping to add.

* Allow the user to choose the game type (human v. human, computer v. computer, human v. computer).
* Allow the user to choose which player goes first.
* Allow the user to choose with what “symbol” the players will mark their selections on the board

(traditionally it’s “X” and “O”). Could you implement these features?

### Code Review Criteria

- Adherence to SOLID principles
- Maintainability, clarity, and readability
- Clear separation of concerns
- Expressive naming

### Approach

* [Project setup](#project-setup)
* [Write user stories](#user-stories)
* [Identify classes and modules to extract from the original `game.rb` file](#classes)
* [Document the design principles applied](#design-principles)
* [Test drive development to fulfil user stories](#development)
* [Update Readme with completed example game](#example-game)

### Project Setup

First of all I set up my Ruby project:

* Create `Gemfile` and initialize RSpec for testing
* Add `.travis.yml` and `Rakefile`
* Add badges to show Travis CI tests, Coveralls code coverage, and Code Climate code quality

### User Stories


```
1.
As a Project Manager
So that the game can be flexible and extendable with new features
I would like the code to be well-written with good design principles

2.
As a Project Manager
So that code can be maintained in the future
I want the code to be well-tested

3.
As a Child
So that I can see if I have entered valid input
I want the game to gracefully handle user input

4.
As a Child
So that I can play a fair game with a better chance of winning
I want the game to have medium difficulty

5.
As a Child
So that I can enjoy following the game play without confusion
I want messages and presentation to be clear

6.
As a Child
So that I can have more choice in my marker
I want to be able to choose my symbol to mark on the board

7.
As a Child
So that I can play by myself, with another, or watch computers play
I want to choose a game type (human v. human, computer v. computer, human v. computer)

8.
As a Child
So that I can choose the order of play
I want to be able to choose which player goes first
```

### Classes

With this project I aimed to create separate modules for the game and CLI. I planned to keep the code similar to the original, and expanded where I can make refactoring improvements or fulfil user stories. My approach was to keep things simple, starting with minimal amounts of classes, then extracting more when necessary.

The `TicTacToe` module includes:

|Game||
|-|-|
|Tracks game status (playing / win / draw)|Rules|
|Has symbols for players||
|Can play with human or computer players|ComputerPlayer||
|Adds symbols to board spaces|||

|ComputerPlayer||
|-|-|
|selects a space on the board|Game|
|uses medium difficulty by default|Rules|
|can set a custom difficulty to make it easier or harder|||

|Rules||
|-|-|
|Checks for win or draw|Game, ComputerPlayer||

The `TicTacToeCLI` module includes:

| GameRunner | |
|-|-|
|Run game loop| Game |
|Prints messages to console, gets user choices|InputOutput|

|GameSetup||
|-|-|
|Prints choice prompts and choice confirmation to console, gets user choices|InputOutput|
|Gathers choices for the user to set up a game|GameRunner, Game|

|BoardView||
|-|-|
|Formats the board for printing|GameRunner, Game|

|InputOutput||
|-|-|
|Handles input and output|GameRunner, GameSetup|

### Design Principles

Here are some of the design principles included in this project, which fulfil [user story 1](#user-stories).

* **Single responsibility principle** I have extracted classes and methods that have clear and separate Responsibilities, for example `Game`, `HumanPlayer`, `Rules` in the `TicTacToe` module.

* **Open-closed principle** The `GameSetup` can easily be extended to add extra configuration for the `Game` instance.

* **Interface segregation principle** The game logic is clearly separated from the interface via the `TicTacToe` and `TicTacToeCLI` modules.

* **Dependency inversion principle** Dependencies are injected into the initialization of classes.

* **Law of Dermeter** I do not chain method calls to objects other than their immediate neighbours.

* **Don't Repeat Yourself** I reused the class `Rules` class in `Game` and `ComputerPlayer`.

* **Readability** I made my code easy to read and maintain via short methods that describe what they do.

### Development

* **Test Driven Development** To fulfil [user story 2](#user-stories) I wrote tests before adding new classes or features. The quality of the code can be monitored through the [test data](https://travis-ci.org/ryanwhocodes/tic_tac_toe_cli), [code coverage](https://coveralls.io/github/ryanwhocodes/tic_tac_toe_cli?branch=master), and [code quality](https://codeclimate.com/github/ryanwhocodes/tic_tac_toe_cli).

* Starting with [user story 6](#user-stories), I added functionality to select custom markers to `Game`.
* Within `Game` I separated methods to print to standard out, separating output from game logic.
* I created `TicTacToe` module to store the game logic and added `Game` to this.
* I removed dependency on input and output within `Game` class, and extracted the printing to console and getting human input to `TicTacToeCLI` module `GameRunner` class
* As I progressed I found I duplicated code with the `Game` class and `ComputerPlayer` class, so I extracted the win and draw checking into a `Rules` class.
* I added a difficulty probability to the `ComputerPlayer`, that can be overridden upon initialization. This means that there is a 50% probability of the best move being chosen, to give this medium difficulty to fulfil [user story 4](#user-stories).
* To fulfil [user story 3](#user-stories), the `GameRunner#get_human_spot` gracefully handles user input by adding `Please try again...` for incorrect input, and `You chose ...` to confirm the user's choice.
* I fulfilled [user stories 6, 7 and 8](#user-stories) by creating a `GameSetup` class for the `TicTacToeCLI`. This asks users for their choices, then stores them, and these are passed into a new instance of `Game` to configure the game for the user.
* To fulfil [user story 5](#user-stories) I added messages to display win or draw, as well as more details about the current player's turn.  
* After some more research I developed the minimax algorithm for the `ComputerPlayer` to use `negamax` with alpha beta pruning to more efficiently find the best move.
